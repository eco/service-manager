<?php

namespace Eco\ServiceManager\Test\Mock {

    use Psr\Container\ContainerInterface;

    class MockService
    {

    }

    class MockServiceFactory implements \Eco\ServiceManager\FactoryInterface
    {

        public function build(ContainerInterface $c)
        {
            return new MockService();
        }

    }

    class MockServiceInvalidFactory implements \Eco\ServiceManager\FactoryInterface
    {

        public function build(ContainerInterface $c)
        {
            // No return is invalid
        }

    }

    class MockServiceFactoryExceptionInConstruct implements \Eco\ServiceManager\FactoryInterface
    {
        public function __construct()
        {
            throw new \RuntimeException('test-exception');
        }

        public function build(ContainerInterface $c)
        {
            return new stdClass;
        }
    }

    class MockServiceFactoryExceptionInCreate implements \Eco\ServiceManager\FactoryInterface
    {
        public function build(ContainerInterface $c)
        {
            throw new \RuntimeException('test-exception');
        }
    }

    class MockServiceExceptionInConstruct
    {
        public function __construct()
        {
            throw new \RuntimeException('test-exception');
        }
    }

}

namespace Eco\ServiceManager\Test {

    use Eco\ServiceManager\Test\Mock\MockService;

    class ServiceManagerTest extends \PHPUnit\Framework\TestCase
    {

        /** @var ServiceManager */
        protected $manager;

        protected function setUp()
        {
            $this->manager = new \Eco\ServiceManager\ServiceManager();
        }

        public function testAddDefinition()
        {
            $this->manager->addServiceDefinition('simple', 'Eco\ServiceManager\Test\Mock\MockService');

            $this->assertTrue($this->manager->has('simple'));
            $this->assertFalse($this->manager->has('SIMPLE'));
        }

        public function testAddDefinitions()
        {
            $definitions = array(
                'simple' => 'Eco\ServiceManager\Test\Mock\MockService',
            );

            $this->manager->addServicesDefinitions($definitions);

            $this->assertTrue($this->manager->has('simple'));
            $this->assertFalse($this->manager->has('SIMPLE'));
        }

        public function testSetServiceSimple()
        {
            $this->manager->setService('test', new MockService());

            $this->assertTrue($this->manager->has('test'));
            $this->assertFalse($this->manager->has('TEST'));
        }

        public function testSetServiceOverwriteThrowException()
        {
            $this->expectException('RuntimeException');

            $this->manager->setService('test', new MockService());
            $this->manager->setService('test', new MockService());
        }

        public function testSetServiceOverwriteOkIfSpecified()
        {
            $this->manager->setService('test', new MockService());
            $this->manager->setService('test', new MockService(), true);

            $this->assertTrue($this->manager->has('test'));
            $this->assertFalse($this->manager->has('TEST'));
        }

        public function testGet()
        {
            $this->manager->addServiceDefinition('test', 'Eco\ServiceManager\Test\Mock\MockService');

            $instance1 = $this->manager->get('test');
            $instance2 = $this->manager->get('test');

            $this->assertSame($instance1, $instance2);
            $this->assertInstanceOf('Eco\ServiceManager\Test\Mock\MockService', $instance1);
        }

        public function testGetThrowExceptionIfNotDefined()
        {
            $this->expectException('RuntimeException', "No service defined with name 'DO NOT EXISTS'");

            $this->manager->get('DO NOT EXISTS');
        }

        public function testGetThrowExceptionIfClassDoesntExists()
        {
            $this->expectException('RuntimeException', "The service class 'NoNs\Class' does not exists");

            $this->manager->addServiceDefinition('test', 'NoNs\Class');

            $this->manager->get('test');
        }

        public function testGetWithFactory()
        {
            $this->manager->addServiceDefinition('test', 'Eco\ServiceManager\Test\Mock\MockServiceFactory');

            $instance = $this->manager->get('test');

            $this->assertInstanceOf('Eco\ServiceManager\Test\Mock\MockService', $instance);
        }

        public function testGetWithInvokable()
        {
            $this->manager->addServiceDefinition('test', 'Eco\ServiceManager\Test\Mock\MockService');

            $instance = $this->manager->get('test');

            $this->assertInstanceOf('Eco\ServiceManager\Test\Mock\MockService', $instance);
        }

        public function testGetWithInvalidFactory()
        {
            $this->expectException('RuntimeException', 'Service instance is null');

            $this->manager->addServiceDefinition('test', 'Eco\ServiceManager\Test\Mock\MockServiceInvalidFactory');

            $this->manager->get('test');
        }

        public function testHasFromDefinition()
        {
            $this->assertFalse($this->manager->has('test'));

            $this->manager->addServiceDefinition('test', 'Eco\ServiceManager\Test\Mock\MockService');

            $this->assertTrue($this->manager->has('test'));
        }

        public function testHasFromInstances()
        {
            $this->assertFalse($this->manager->has('test'));

            $this->manager->addServiceDefinition('test', 'Eco\ServiceManager\Test\Mock\MockService');

            $this->manager->get('test');

            $this->assertTrue($this->manager->has('test'));
        }

        public function testCyclicDependencyFactoryWithExceptionInConstruct()
        {
            $this->manager->addServiceDefinition('cyclic-test', 'Eco\ServiceManager\Test\Mock\MockServiceFactoryExceptionInConstruct');

            try {
                $exceptionCatched = false;
                $this->manager->get('cyclic-test');
            } catch (\Exception $e) {
                $exceptionCatched = true;
            }

            $this->assertTrue($exceptionCatched, 'An exception must have been thrown');

            try {
                $this->manager->get('cyclic-test');
            } catch (\RuntimeException $e) {
                $this->assertSame('test-exception', $e->getMessage());

                return;
            }
            $this->fail('incorrect exception thrown');
        }

        public function testCyclicDependencyFactoryWithExceptionInCreate()
        {
            $this->manager->addServiceDefinition('cyclic-test', 'Eco\ServiceManager\Test\Mock\MockServiceFactoryExceptionInCreate');

            try {
                $exceptionCatched = false;
                $this->manager->get('cyclic-test');
            } catch (\Exception $e) {
                $exceptionCatched = true;
            }

            $this->assertTrue($exceptionCatched, 'An exception must have been thrown');

            try {
                $this->manager->get('cyclic-test');
            } catch (\RuntimeException $e) {
                $this->assertSame('test-exception', $e->getMessage());

                return;
            }
            $this->fail('incorrect exception thrown');
        }

        public function testCyclicDependencyWithExceptionInConstruct()
        {
            $this->manager->addServiceDefinition('cyclic-test', 'Eco\ServiceManager\Test\Mock\MockServiceExceptionInConstruct');

            try {
                $exceptionCatched = false;
                $this->manager->get('cyclic-test');
            } catch (\Exception $e) {
                $exceptionCatched = true;
            }

            $this->assertTrue($exceptionCatched, 'An exception must have been thrown');

            try {
                $this->manager->get('cyclic-test');
            } catch (\RuntimeException $e) {
                $this->assertSame('test-exception', $e->getMessage());

                return;
            }
            $this->fail('incorrect exception thrown');
        }

    }

}