<?php
/** @var \Composer\Autoload\ClassLoader $loader */
$loader = require __DIR__ . '/../vendor/autoload.php';

$loader->addPsr4('Eco\\ServiceManager\\Test\\', 'src/');
