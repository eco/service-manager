<?php

namespace Eco\ServiceManager;

interface ServiceLocatorInterface
{
    /**
     * Retrieve a registered instance
     *
     * @param string $name
     * @return mixed
     */
    public function get(string $name);

    /**
     * Check for a registered instance
     *
     * @param string $name
     * @return bool
     */
    public function has(string $name): bool;
}
