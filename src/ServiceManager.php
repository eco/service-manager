<?php

namespace Eco\ServiceManager;

use Psr\Container\ContainerInterface;

class ServiceManager implements ContainerInterface
{
    /** @var array */
    protected $definitions = [];

    /** @var array */
    protected $instances = [];

    /** @var array */
    protected $currentlyBuildingServices = [];

    public function __construct(array $definitions = [])
    {
        $this->addServicesDefinitions($definitions);
    }

    public function addServicesDefinitions(array $definitions)
    {
        foreach ($definitions as $name => $definition) {
            if (is_string($definition)) {
                $this->addServiceDefinition($name, $definition);
            } else if (is_array($definition)) {
                $this->setService($name, $definition);
            } else {
                throw new \RuntimeException("Invalid service definition (should be a class name or an array)");
            }
        }

        return $this;
    }

    public function addServiceDefinition(string $cName, string $className)
    {
        if (isset($this->definitions[$cName])) {
            throw new \RuntimeException('That definition already exists');
        }

        $this->definitions[$cName] = $className;

        return $this;
    }

    public function setService(string $cName, $instance, $overwrite = false)
    {
        if ($instance === null) {
            throw new \RuntimeException("Service instance is null");
        }

        if (!$overwrite && isset($this->instances[$cName])) {
            throw new \RuntimeException('An instance with the same name already exists');
        }

        $this->instances[$cName] = $instance;

        return $this;
    }

    public function get($cName)
    {
        if (isset($this->instances[$cName])) {
            return $this->instances[$cName];
        }

        if (!isset($this->definitions[$cName])) {
            throw new \RuntimeException("No service defined with name '" . $cName . "'");
        }

        if (!class_exists($this->definitions[$cName])) {
            throw new \RuntimeException("The service class '" . $this->definitions[$cName] . "' does not exists");
        }

        if (isset($this->currentlyBuildingServices[$cName])) {
            throw new \RuntimeException('Cyclic dependency detected');
        }
        $this->currentlyBuildingServices[$cName] = true;

        try {
            // Try to create an instance of the service
            $instance = new $this->definitions[$cName]($this);

            // If it is a factory we will call 'build' on it
            if ($instance instanceof FactoryInterface) {
                $serviceInstance = $instance->build($this);
            } else {
                $serviceInstance = $instance; // Invokable
            }

            $this->setService($cName, $serviceInstance);
        } catch (\Exception $e) {
            unset($this->currentlyBuildingServices[$cName]);
            throw $e;
        }

        unset($this->currentlyBuildingServices[$cName]);

        return $this->instances[$cName];
    }

    public function has($cName): bool
    {
        if (isset($this->instances[$cName])) {
            return true;
        }

        if (isset($this->definitions[$cName])) {
            return true;
        }

        return false;
    }

}
